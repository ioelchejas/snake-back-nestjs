import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { OnlineEventRequest, OnlineEventResponse, OnlineEventResponseType } from './interfaces/online.interface';

@WebSocketGateway(8443, {
  cors: {
    origin: ['http://localhost:4200', 'https://snake-game-online.web.app'],
    credentials: true,
    methods: ["GET", "POST"],
    transports: ['websocket', 'polling'],
  },
  allowEIO3: true,
})
export class ChatGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  // create matrix map
  roomMap = new Map<string, string[]>();

  afterInit(server: any) {
    console.log('INIT: Esto se ejecuta cuando inicia', this.server)
    this.server.on('close', () => {
      console.log("WEB SOCKET CLOSED")
    })
  }

  handleConnection(client: any, ...args: any[]) {
    console.log('👌👌👌 CONNECTION: Hola alguien se conecto al socket 👌👌👌');
  }

  handleDisconnect(client: any) {
    console.log('👋👋👋 DISCONECT: ALguien se fue! chao chao 👋👋👋')
  }


  @SubscribeMessage('EVENT_JOIN')
  handleJoinRoom(client: Socket, event: OnlineEventRequest) {
    this.server.in(event.room).fetchSockets()
      .then((sockets) => {
        console.log("CLIENTS: ", sockets.length)
        if (sockets.length < 2) {
          client.join(event.room);

          this.roomMap.set(
            event.room, this.roomMap.get(event.room)
            ?
            [...this.roomMap.get(event.room),
            event.userId
            ]
            : [
              event.userId
            ]
          )
          console.log(this.roomMap.get(event.room))
          this.server.to(event.room).emit("USER_JOIN_TO_GAME", event.userId);

          if (sockets.length === 1) {
            this.server.to(event.room).emit<OnlineEventResponseType>("START", this.roomMap.get(event.room));
          }

          client.on('disconnecting', (e) => {
            this.roomMap.get(event.room).splice(this.roomMap.get(event.room).indexOf(event.userId), 1);
            this.server.to(event.room).emit("USER_LEFT_THE_GAME", event.userId);
            console.log("disconnecting from room", event.room, event.userId, this.roomMap.get(event.room))
          })

        }
        else {
          client.emit("CANT_JOIN_ROOM_COMPLETE", event.userId);
        }
      })
    //  .catch(err => console.log("ERR ON GET ROOM SOCKETS"))
    // return from(room).pipe(map(
    //   (res: any) => {
    //     return {
    //       event: 'meeting',
    //       data: room
    //     }
    //   }
    // ))
  }

  @SubscribeMessage('GAME_EVENT') //TODO Backend
  handleIncommingMessage(
    client: Socket,
    payload: OnlineEventRequest,
  ) {
    const { room, gameEvent, userId } = payload;
    console.log(payload)
    const res: OnlineEventResponse = {
      gameEvent: gameEvent as OnlineEventResponseType,
      userId,
    }
    this.server.to(room).emit<string>('GAME_EVENT', res);
  }

  @SubscribeMessage('EVENT_LEAVE')
  handleRoomLeave(client: Socket, event: OnlineEventRequest) {
    if (event.room) {
      client.leave(event.room);
      this.roomMap.get(event.room).splice(this.roomMap.get(event.room).indexOf(event.userId), 1);
      this.server.to(event.room).emit("USER_LEFT_THE_GAME", event.userId);
      console.log("SUB: ABANDONO A ROOM ", event.room)
    }
  }
}