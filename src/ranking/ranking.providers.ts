import { Connection } from 'mongoose';
import { RankingSchema } from './schemas/user.schema';

export const rankingProviders = [
  {
    provide: 'RANKING_MODEL',
    useFactory: (connection: Connection) => connection.model('Ranking', RankingSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];