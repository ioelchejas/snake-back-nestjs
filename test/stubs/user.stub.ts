import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UpdateScoreDto } from 'src/users/dto/update-score.dto';

export const userCreateDTOStub = (): CreateUserDto => {
  return {
    email: 'nicolasferreroutn@gmail.com',
    lastName: 'Ferrero',
    name: 'Nicolas',
    password: '12345678',
    roles: ['user'],
  };
};

export const updateScoreDTOStub = (): UpdateScoreDto => {
  return {
    id: '633a13fe144ac56dbbaa69b8',
    score: 71,
  };
};
