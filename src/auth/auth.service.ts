import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Response } from 'express';
import { User } from 'src/users/interfaces/user.interface';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USER_MODEL')
    private userModel: Model<User>,
    private jwtService: JwtService
  ) { }

  async signUp(signUpDto: SignUpDto, response: Response): Promise<any> {
    try {
      const userFound = await this.userModel.findOne({ email: signUpDto.email });
      if (userFound) {
        return response.status(HttpStatus.CONFLICT).json({error: "Usted ya se encuentra registrado, pruebe iniciando sesión"});
      }

      const newUser = new this.userModel({ ...signUpDto, roles: ['user'], score: 0 });
      newUser.password = await newUser.encryptPassword(signUpDto.password);

      const savedUser = await newUser.save();
      const token = this.jwtService.sign({ id: savedUser._id, roles: savedUser.roles }, {
        secret: process.env.SECRET_JWT as string,
        expiresIn: 84600, //24hs
      });

      return response.status(HttpStatus.CREATED).json(token);
    } catch (error) {
      console.log(error)
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
    }
  }

  async signIn(signInDto: SignInDto, response: Response): Promise<any> {
    try {
      const userFound = await this.userModel.findOne({ email: signInDto.email });
      if (!userFound) return response.status(HttpStatus.UNAUTHORIZED).json({error: "Credenciales inválidas"});
      const matchPassword = await userFound.comparePasswords(signInDto.password, userFound.password as string);
      if (!matchPassword) return response
        .status(HttpStatus.UNAUTHORIZED)
        .json({ error: 'Credenciales inválidas' });
      const token = this.jwtService.sign({ id: userFound._id, roles: userFound.roles }, {
        secret: process.env.SECRET_JWT as string,
        expiresIn: 84600, //24hs,
      });
      return response.status(HttpStatus.CREATED).json(token);
    } catch (error) {
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
    }
  }

}