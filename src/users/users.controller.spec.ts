import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { RolesGuard } from '../../src/guards/roles.guard';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { Response } from 'express'
import { createResponse } from 'node-mocks-http';
import mongoose from 'mongoose';
import { userCreateDTOStub, updateScoreDTOStub } from '../../test/stubs/user.stub';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateScoreDto } from './dto/update-score.dto';

describe('UserController', () => {
  let userController: UsersController;
  const mockUsersService = {
    create: jest.fn().mockImplementation((createUserDto: CreateUserDto, response: Response) => {
      return {
        _id: new mongoose.Types.ObjectId(),
        ...createUserDto,
        score: 0
      }
    }),
    findByIdAndUpdateScore: jest.fn().mockImplementation((response: Response, updateScoreDto: UpdateScoreDto) => {
      return {
        _id: '633a13fe144ac56dbbaa69b8',
        name: 'Nicolas',
        lastName: 'Ferrero',
        email: 'nicolasferreroutn@gmail.com',
        password:
          '$2a$10$v5aHUM6XQ4/guFSzPbo1wuB1KiCtZfR6SoxDd416W/QZSF6r3VP7y',
        score: updateScoreDto.score,
        roles: ['user'],
      };
    })
  };
  const mockResponse = createResponse()

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: RolesGuard,
          useValue: jest.fn().mockImplementation(() => true),
        },
        {
          provide: JwtService,
          useValue: jest.fn().mockImplementation(() => true),
        },
      ],
    })
      .overrideProvider(UsersService)
      .useValue(mockUsersService)
      .compile();

    userController = moduleRef.get<UsersController>(UsersController);
  });

  it('Should be defined', () => {
    expect(userController).toBeDefined();
  });

  it('Should create user', async () => {
    const user = await userController.create(mockResponse, userCreateDTOStub());
    expect(user).toEqual({
      _id: expect.any(mongoose.Types.ObjectId),
      ...userCreateDTOStub(),
      score: expect.any(Number),
    });
  });

  it('Should update user score', async () => {
    const user = await userController.updateScore(mockResponse, updateScoreDTOStub())
    expect(user.score).toEqual(updateScoreDTOStub().score);
  });
});
