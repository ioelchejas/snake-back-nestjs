import { Body, Controller, Post, Res, UseGuards, ValidationPipe} from '@nestjs/common';
import { Response } from 'express';
import { RolesGuard } from 'src/guards/roles.guard';
import { User } from 'src/users/interfaces/user.interface';
import { SignUpDto } from './dto/sign-up.dto';
import { AuthService } from './auth.service';
import { SignInDto } from './dto/sign-in.dto';

@Controller('auth')
@UseGuards(RolesGuard)
export class AuthController {

    constructor(private authService: AuthService) {}

    @Post('signUp')
    async signUp(@Res() response: Response<User>, @Body(new ValidationPipe()) signUpDto: SignUpDto): Promise<any> {
        console.log("LOGEANDOSE...")
        await this.authService.signUp(signUpDto, response);
    }

    @Post('signIn')
    async signIn(@Res() response: Response<string>, @Body(new ValidationPipe()) signInDto: SignInDto): Promise<any> {
        await this.authService.signIn(signInDto, response);
    }

}
