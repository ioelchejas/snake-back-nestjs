import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { Response } from 'express';
import { UpdateScoreDto } from './dto/update-score.dto';

@Injectable()
export class UsersService {
  constructor(
    @Inject('USER_MODEL')
    private userModel: Model<User>,
  ) { }

  async create(createUserDto: CreateUserDto, response: Response): Promise<any> {
    try {
      const userFound = await this.userModel.findOne({ email: createUserDto.email });
      if (userFound) {
        return response.status(HttpStatus.CONFLICT).json({ user: null, error: 'Ya existe un usuario creado con ese email' });
      }

      const newUser = new this.userModel({ ...createUserDto, score: 0 });
      newUser.password = await newUser.encryptPassword(createUserDto.password);

      const savedUser = await newUser.save();
      return response.status(HttpStatus.CREATED).json(savedUser);
    } catch (error) {
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
    }
  }


  async findAll(response: Response, page: number, limit: number): Promise<any> {
    try {
      const users = await this.userModel.find()
        .skip(page * limit)
        .limit(limit);
      return response.status(HttpStatus.OK).json(users);
    } catch (error) {
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
    }
  }

  async findById(response: Response, id: string): Promise<any> {
    try {
      const user = await this.userModel.findById(id);
      if (!user) {
        return response.status(HttpStatus.NOT_FOUND).json({ user: null, error: 'El usuario no existe' });
      }
      return response.status(HttpStatus.OK).json(user);
    } catch (error) {
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
    }
  }

  async findByIdAndUpdateScore(response: Response, updateScoreDto: UpdateScoreDto): Promise<any> {
    try {
      const user = await this.userModel.findById(updateScoreDto.id);
      if (!user) {
        return response.status(HttpStatus.NOT_FOUND).json({ user: null, error: 'El usuario no existe' });
      }
      if (user.score >= updateScoreDto.score) {
        return response.status(HttpStatus.BAD_REQUEST).json({ user: null, error: 'El puntaje no supera el actual' });
      }
      const updatedUser = await this.userModel.findByIdAndUpdate(user._id, { 
        name: user.name,
        lastName: user.lastName,
        email: user.email,
        password: user.password,
        roles: user.roles,
        score: updateScoreDto.score
       }, { new: true });
      return response.status(HttpStatus.OK).json(updatedUser);
    } catch (error) {
      return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
    }
  }

}