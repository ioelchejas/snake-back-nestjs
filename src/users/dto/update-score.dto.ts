import { IsString, IsInt } from 'class-validator';

export class UpdateScoreDto {
  @IsString()
  id: string;

  @IsInt()
  score: number;
}