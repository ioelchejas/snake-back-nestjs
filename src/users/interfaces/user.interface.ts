import { Document } from 'mongoose';

export interface User extends Document{
    name: string;
    lastName: string;
    email: string;
    password: string;
    score: number;
    roles: string[];

    encryptPassword(password: string): Promise<string>;
    comparePasswords(password: string, receivedPassword: string): Promise<boolean>;
}