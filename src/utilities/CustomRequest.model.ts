import { Request } from 'express';

export interface RequestCustomModel extends Request {
  id: string;
}
