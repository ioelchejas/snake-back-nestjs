import { IsInt } from 'class-validator';

export class CreateRankingDto {
  @IsInt()
  score: number;
}
