<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>
  
# Proyecto juego snake

Este repositorio contiene el backend del proyecto del juego Snake.

## Tecnologias utilizadas

Para el proyecto decidimos utilizar las siguientes tecnologias

| Tecnologias                                                                                                                | Página oficial          |
| -------------------------------------------------------------------------------------------------------------------------- | ----------------------- |
| ![NestJs](https://img.shields.io/badge/NestJs-0e0e10?style=flat&logo=NestJS&logoColor=E0234E)                              | https://nestjs.com      |
| ![MongoDB](https://img.shields.io/badge/MongoDB-0e0e10?style=flat&logo=MongoDB&logoColor=47A248)                           | https://www.mongodb.com |
| ![mongoose](https://img.shields.io/badge/mongoose-0e0e10?style=flat&logo=MongoDB&logoColor=47A248)                         | https://www.mongodb.com |
| ![JSON Web Tokens](https://img.shields.io/badge/JSON%20Web%20Tokens-0e0e10?style=flat&logo=JSONWebTokens&logoColor=d539ff) | https://jwt.io          |
| ![Jest](https://img.shields.io/badge/Jest-black?style=flat&logo=Jest&logoColor=15c213)                                     | https://jestjs.io/      |
| ![Angular](https://img.shields.io/badge/Angular-black?style=flat&logo=Angular&logoColor=DD0031)                            | https://angular.io/     |

## API Reference

#### Sign Up

```http
  POST /auth/signUp
```

| Body       | Type     | Description  |
| :--------- | :------- | :----------- |
| `name`     | `string` | **Required** |
| `lastName` | `string` | **Required** |
| `email`    | `string` | **Required** |
| `password` | `string` | **Required** |

| Header | Type | Description |
| :----- | :--- | :---------- |
| `none` | N/C  | N/C         |

#### Sign In

```http
  POST /auth/signIn
```

| Body       | Type     | Description  |
| :--------- | :------- | :----------- |
| `email`    | `string` | **Required** |
| `password` | `string` | **Required** |

| Header | Type | Description |
| :----- | :--- | :---------- |
| `none` | N/C  | N/C         |

#### Get list of user's ranking

```http
  GET /ranking
```

| Parameter | Type | Description |
| :-------- | :--- | :---------- |
| `none`    | N/C  | N/C         |

| Header          | Type        | Description  |
| :-------------- | :---------- | :----------- |
| `Authorization` | `JWT Token` | **Required** |

#### Submit new score to ranking

```http
  POST /ranking
```

| Body    | Type  | Description  |
| :------ | :---- | :----------- |
| `score` | `Int` | **Required** |

| Header          | Type        | Description  |
| :-------------- | :---------- | :----------- |
| `Authorization` | `JWT Token` | **Required** |

#### Update user's highest score

```http
  POST /users
```

| Body    | Type                | Description  |
| :------ | :------------------ | :----------- |
| `score` | `Int`               | **Required** |
| `id`    | `Mongoose ObjectID` | **Required** |

| Header          | Type        | Description  |
| :-------------- | :---------- | :----------- |
| `Authorization` | `JWT Token` | **Required** |

#### Get list of users [ADMIN ONLY]

```http
  GET /users
```

| Parameter | Type | Description |
| :-------- | :--- | :---------- |
| `none`    | N/C  | N/C         |

| Header          | Type        | Description  |
| :-------------- | :---------- | :----------- |
| `Authorization` | `JWT Token` | **Required** |

#### Get user [ADMIN ONLY]

```http
  GET /users/${id}
```

| Parameter | Type                  | Description  |
| :-------- | :-------------------- | :----------- |
| `id`      | ``Mongoose ObjectID`` | **Required** |

| Header          | Type        | Description  |
| :-------------- | :---------- | :----------- |
| `Authorization` | `JWT Token` | **Required** |

#### Create user [ADMIN ONLY]

```http
  POST /users
```

| Body       | Type       | Description  |
| :--------- | :--------- | :----------- |
| `name`     | `string`   | **Required** |
| `lastName` | `string`   | **Required** |
| `email`    | `string`   | **Required** |
| `password` | `string`   | **Required** |
| `roles`    | `[string]` | **Required** |

| Header          | Type        | Description  |
| :-------------- | :---------- | :----------- |
| `Authorization` | `JWT Token` | **Required** |

## Usuarios de prueba

| Email                         | Paassword  | Role    |
| :---------------------------- | :--------- | :------ |
| `ioelchejas@gmail.com`        | `12345`    | `Admin` |
| `nicolasferreroutn@gmail.com` | `12345678` | `User`  |

## Installation

```bash
$ npm install
```

## Variables de entorno archivo .env
```
PORT=<port>
MONGO_URI='mongodb+srv://<cluster>:<pass>@cluster0.iwau3.mongodb.net/<dbname>'
SECRET_JWT='<frasesecreta>'
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Deployar en Render
https://dashboard.render.com/

Para deployar en render solo hay que hacer un push a master.

## Deployar en heroku
Mirar https://shivamv12.medium.com/deploy-nestjs-on-heroku-in-5-simple-steps-cc7625ea6167

### Agregar Procfile
```
web: npm run start:prod
```

### Agregar en el package json en el primer nivel 
```json
"engines": {
  "node": "14.10.1"
},
```

### Usar puerto configurado 
```typescript
await app.listen(process.env.PORT || 8080);
```

### Deployar primera vez en heroku
```bash
$ heroku login
$ git init
$ heroku git:remote -a snake-back-nestjs
$ git add .
$ git commit -m "Deploying on heroku"
$ git push heroku master
```

### Configurar las variables de entorno
```bash
$ heroku config:set PORT=<port>
$ heroku config:set MONGO_URI='mongodb+srv://<cluster>:<pass>@cluster0.iwau3.mongodb.net/<dbname>'
$ heroku config:set SECRET_JWT='<frasesecreta>'
```

### Deployar en heroku
```bash
# En caso de ser necesario
$ heroku login

$ git add .
$ git commit -m "Deploying on heroku"
$ git push heroku master
```

## Desarrolladores

- [Ioel Itzjak Chejas](https://www.github.com/octokatherine)
- [Nicolás A. Ferrero](https://www.github.com/octokatherine)


