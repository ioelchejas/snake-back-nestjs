import { Body, Controller, Get, Param, Post, Put, Query, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { RolesGuard } from '../guards/roles.guard';
import { User } from 'src/users/interfaces/user.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { Roles } from '../decorators/roles.decorator';
import { ValidationPipe } from './pipes/user-validation.pipe';
import { UpdateScoreDto } from './dto/update-score.dto';

@Controller('users')
@UseGuards(RolesGuard)
export class UsersController {

    constructor(private usersService: UsersService) { }

    @Get()
    @Roles('admin')
    async findAll(@Res() response: Response<User[]>, @Query('page') page: number, @Query('limit') limit: number): Promise<any> {
        return await this.usersService.findAll(response, page, limit);
    }

    @Get(':id')
    //@Roles('admin')
    async findOne(@Res() response: Response, @Param('id') id: string): Promise<any> {
        return await this.usersService.findById(response, id)
    }

    @Post()
    @Roles('admin')
    async create(@Res() response: Response, @Body(new ValidationPipe()) createUserDto: CreateUserDto): Promise<any> {
        return await this.usersService.create(createUserDto, response);
    }

    @Put()
    async updateScore(@Res() response: Response, @Body(new ValidationPipe()) updateScoreDto: UpdateScoreDto): Promise<any> {
        return await this.usersService.findByIdAndUpdateScore(response, updateScoreDto);
    }
}
