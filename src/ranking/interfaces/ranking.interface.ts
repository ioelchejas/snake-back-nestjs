import { Document } from 'mongoose';

export interface Ranking extends Document{
    email: string;
    score: number;
}