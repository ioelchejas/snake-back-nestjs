import { CreateRankingDto } from 'src/ranking/dto/create-ranking.dto';

export const CreateRankingDTOStub = (): CreateRankingDto => {
  return {
    score: 50,
  };
};
