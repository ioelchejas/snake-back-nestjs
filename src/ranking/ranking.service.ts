import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Response } from 'express';
import { Ranking } from './interfaces/ranking.interface';
import { CreateRankingDto } from './dto/create-ranking.dto';
import { RequestCustomModel } from 'src/utilities/CustomRequest.model';
import { User } from 'src/users/interfaces/user.interface';

@Injectable()
export class RankingService {
    private readonly MAX_RANKING_REGISTER = 10;

    constructor(
        @Inject('RANKING_MODEL')
        private rankingModel: Model<Ranking>,
        @Inject('USER_MODEL')
    private userModel: Model<User>
    ) { }

    async create(request: RequestCustomModel, createRankingDto: CreateRankingDto, response: Response): Promise<any> {
        try {
            const user = await this.userModel.findOne({ _id: request.id });
            const cant = await this.rankingModel.count();
            if (cant < this.MAX_RANKING_REGISTER) {
                const newRanking = new this.rankingModel(createRankingDto);
                const savedRanking = await newRanking.save();
                return response.status(HttpStatus.CREATED).json(savedRanking);
            }
            const minRanking = await this.rankingModel.find().sort({ score: 1 }).limit(1);
            if (minRanking[0].score < createRankingDto.score) {
                const updatedRanking = await this.rankingModel.findByIdAndUpdate(minRanking[0]._id, {
                    email: user.email,
                    score: createRankingDto.score
                }, { new: true });
                return response.status(HttpStatus.CREATED).json(updatedRanking);
            }
            return response.status(HttpStatus.BAD_REQUEST).json({ error: 'No se supero ningun ranking' });
        } catch (error) {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
        }
    }

    async findAll(response: Response): Promise<any> {
        try {
            const ranking = await this.rankingModel.find().sort({ score: -1 });
            return response.status(HttpStatus.OK).json(ranking);
        } catch (error) {
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ error: 'Internal Server error' });
        }
    }

}