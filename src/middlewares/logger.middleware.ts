import { HttpStatus, Inject, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { User } from 'src/users/interfaces/user.interface';
import { RequestCustomModel } from 'src/utilities/CustomRequest.model';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {

  constructor(
    @Inject('USER_MODEL')
    private userModel: Model<User>,
    private jwtService: JwtService
  ) { }

  async use(request: RequestCustomModel, response: Response, next: NextFunction) {
    try {
      const token = request.headers.authorization;
      if (!token) return response.status(HttpStatus.FORBIDDEN).json({ error: 'No se envio un token' });
      const payload = this.jwtService.verify(token, { secret: process.env.SECRET_JWT as string }) as { id: string; roles: string[] };
      const user = await this.userModel.findById(payload.id);
      if (!user) return response.status(HttpStatus.FORBIDDEN).json({ error: 'El usuario no existe' });
      request.id = payload.id;
      next();
    } catch (error) {
      return response.status(HttpStatus.UNAUTHORIZED).json({ error: 'No autorizado' });
    }
  }
}
