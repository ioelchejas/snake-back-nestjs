import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { LoggerMiddleware } from './middlewares/logger.middleware';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { usersProviders } from './users/users.providers';
import { DatabaseModule } from './database/database.module';
import { JwtModule } from '@nestjs/jwt';
import { UsersController } from './users/users.controller';
import { RankingModule } from './ranking/ranking.module';
import { RankingController } from './ranking/ranking.controller';
import { ChatGateway } from './chat/chat.gateway';

@Module({
  imports: [
    JwtModule.register({}),
    DatabaseModule,
    ConfigModule.forRoot(),
    UsersModule,
    AuthModule,
    RankingModule,
    ChatGateway
  ],
  providers: [
    ...usersProviders
],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes(UsersController, RankingController)
  }
}
