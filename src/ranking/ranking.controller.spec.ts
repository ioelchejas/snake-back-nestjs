import { Test, TestingModule } from '@nestjs/testing';
import { Response } from 'express';
import { createResponse, createRequest } from 'node-mocks-http';
import { RankingController } from './ranking.controller';
import { RankingService } from './ranking.service';
import { RequestCustomModel } from '../../src/utilities/CustomRequest.model';
import { CreateRankingDto } from './dto/create-ranking.dto';
import { CreateRankingDTOStub } from 'test/stubs/ranking.stub';

describe('UserController', () => {
  let rankingController: RankingController;
  const mockRankingService = {
    create: jest
      .fn()
      .mockImplementation(
        (
          request: RequestCustomModel,
          createRankingDto: CreateRankingDto,
          response: Response,
        ) => {
          return {
            _id: '633a13fe144ac56dbbaa69b8',
            email: 'nicolasferreroutn@gmail.com',
            score: createRankingDto.score,
          };
        },
      ),
  };
  const mockResponse = createResponse();
  const mockRequest = createRequest() as RequestCustomModel;
  mockRequest.id = '633a13fe144ac56dbbaa69b8';

  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [RankingController],
      providers: [RankingService],
    })
      .overrideProvider(RankingService)
      .useValue(mockRankingService)
      .compile();

    rankingController = moduleRef.get<RankingController>(RankingController);
  });

  it('Should be defined', () => {
    expect(rankingController).toBeDefined();
  });

  it('Should submit new score to ranking', async () => {
    const user = await rankingController.create(
      mockRequest,
      mockResponse,
      CreateRankingDTOStub(),
    );
    expect(user.score).toEqual(CreateRankingDTOStub().score);
  });
});
