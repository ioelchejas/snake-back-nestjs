import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { RankingController } from './ranking.controller';
import { rankingProviders } from './ranking.providers';
import { RankingService } from './ranking.service';

@Module({
    imports: [
        JwtModule.register({}),
      ],
      controllers: [RankingController],
      providers: [RankingService,
      ...rankingProviders
      ],
      exports: [
        RankingService,
        ...rankingProviders
      ]
})
export class RankingModule {}
