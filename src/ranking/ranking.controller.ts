import { Body, Controller, Get, Post, Req, Res, ValidationPipe } from '@nestjs/common';
import { Response } from 'express';
import { RequestCustomModel } from 'src/utilities/CustomRequest.model';
import { CreateRankingDto } from './dto/create-ranking.dto';
import { Ranking } from './interfaces/ranking.interface';
import { RankingService } from './ranking.service';

@Controller('ranking')
export class RankingController {

    constructor(private rankingService: RankingService) { }

    @Post()
    async create(@Req() request: RequestCustomModel, @Res() response: Response, @Body(new ValidationPipe()) createRankingDto: CreateRankingDto): Promise<any> {
        return await this.rankingService.create(request, createRankingDto, response);
    }

    @Get()
    async findAll(@Res() response: Response<Ranking[]>): Promise<any> {
        await this.rankingService.findAll(response);
    }

}
