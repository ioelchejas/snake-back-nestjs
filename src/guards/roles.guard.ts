import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private reflector: Reflector,
        private jwtService: JwtService
    ) { }

    canActivate(context: ExecutionContext): boolean {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());
        if (!roles) {
            return true;
        }
        const request: Request = context.switchToHttp().getRequest();
        const decoded = this.jwtService.decode(request.headers.authorization)
        console.log(decoded)
        return this.matchRoles(roles, (decoded as any).roles);
    }

    private matchRoles(allowedRoles: string[], userRoles: string[]): boolean {
        return userRoles.find(userRole => allowedRoles.find(allowedRole => allowedRole === userRole)) ? true : false;
    }
}
