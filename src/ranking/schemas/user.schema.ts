import * as mongoose from 'mongoose';

const RankingSchema = new mongoose.Schema({
  email: { type: String, required: true },
  score: { type: Number, required: true },
});

export { RankingSchema }